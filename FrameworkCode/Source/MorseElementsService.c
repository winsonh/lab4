/****************************************************************************
 Module
   MorseElementsService.c

Pseudo-code for the Morse Elements module (a service that implements a state 
machine)Data private to the module: MyPriority, CurrentState, TimeOfLastRise, 
TimeOfLastFall, LengthOfDot, FirstDelta, LastInputState
****************************************************************************/
// #define TEST

// the common headers for C99 types 
#include <stdint.h>
#include <stdbool.h>
#include <termio.h>


// the headers to access the GPIO subsystem
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_sysctl.h"

// the headers to access the TivaWare Library
#include "driverlib/sysctl.h"
#include "driverlib/pin_map.h"
#include "driverlib/gpio.h"
#include "driverlib/timer.h"
#include "driverlib/interrupt.h"

/* include header files for the framework
*/
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "ES_DeferRecall.h"
#include "ES_ShortTimer.h"

#include "BITDEFS.H"

#include "MorseElementsService.h"
#include "DecodeMorse.h"

// readability defines
#define ALL_BITS (0xff << 2)

/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this service.They should be functions
   relevant to the behavior of this service
*/
void TestCalibration(void);
void CharacterizeSpace(void);
void CharacterizePulse(void);


/*---------------------------- Module Variables ---------------------------*/
// with the introduction of Gen2, we need a module level Priority variable
static uint8_t MyPriority;
static MorseElementState_t CurrentState;
static uint16_t TimeOfLastRise;
static uint16_t TimeOfLastFall; 
static uint8_t LengthOfDot;
static uint16_t FirstDelta;
static uint8_t LastInputState;


/*------------------------------ Module Code ------------------------------*/

/*
*/
bool InitializeMorseElements(uint8_t Priority) {
  ES_Event_t ThisEvent;
  
	// Initialize the MyPriority variable with the passed in parameter.
  MyPriority = Priority;
	
	/*
	Initialize the port line to receive Morse codeSample port line
	*/
	// Port Initialization
  HWREG(SYSCTL_RCGCGPIO) |= SYSCTL_RCGCGPIO_R1; // Enable Port B
  while ( (HWREG(SYSCTL_PRGPIO) & SYSCTL_PRGPIO_R1) != SYSCTL_PRGPIO_R1){
  }
  
  // Digital Port Selection
  HWREG(GPIO_PORTB_BASE + GPIO_O_DEN) |= BIT3HI; // Make bit 3 of Port B to be digital
  
  // Data Direction Setting
  HWREG(GPIO_PORTB_BASE + GPIO_O_DIR) &= BIT3LO; // Make bit 3 of Port B to be intput
  
  // Set bit 3 low.
  HWREG(GPIO_PORTB_BASE + (GPIO_O_DATA + ALL_BITS)) &= BIT3LO; // |=  Make bit 3 of Port B to be low
	
	// Sample port line and use it to initialize the LastInputState variable
	LastInputState = HWREG(GPIO_PORTB_BASE + (GPIO_O_DATA + ALL_BITS)) & BIT3HI;
	
	// Set CurrentState to be InitMorseElements 
	CurrentState = InitMorseElements; 
	
	// Set Set FirstDelta to 0 
	FirstDelta = 0;
	
	// Post Event ES_Init to MorseElements queue (this service)
	ThisEvent.EventType = ES_INIT;
  if (ES_PostToService( MyPriority, ThisEvent) == true)
  {  
    return true;
  }else
  {
      return false;
  }
	
	// End of InitializeMorseElements 
}




/*
*/
bool CheckMorseEvents(void) {;
  bool ReturnVal = false;
	static uint8_t CurrentInputState;
	
	// Get the CurrentInputState from the input line
	CurrentInputState = HWREG(GPIO_PORTB_BASE + (GPIO_O_DATA + ALL_BITS)) & BIT3HI;
	
	// If the state of the Morse input line has changed
	if(CurrentInputState != LastInputState) {
		
		// If the current state of the input line is high
				// PostEvent RisingEdge with parameter of the Current Time.
		// Else (Current input state is low)
				// PostEvent FallingEdge with parameter of the Current Time.
		if(CurrentInputState != 0) {
			
      ES_Event_t ThisEvent;
			ThisEvent.EventType = RisingEdge;
			ThisEvent.EventParam = ES_Timer_GetTime();
			// printf("R");			
			// printf(" %d\n", ThisEvent.EventParam);
      ES_PostToService( MyPriority, ThisEvent);
		}
		else {
			ES_Event_t ThisEvent;
			ThisEvent.EventType = FallingEdge;
			ThisEvent.EventParam = ES_Timer_GetTime();
			// printf("F");
      // printf(" %d\n", ThisEvent.EventParam);
      ES_PostToService( MyPriority, ThisEvent);
		}
    ReturnVal = true;
	}
    
		LastInputState = CurrentInputState;
		return ReturnVal;
}




/*
*/
ES_Event_t RunMorseElementsSM( ES_Event_t ThisEvent ) {
	struct ES_Event ReturnEvent;
  ReturnEvent.EventType = ES_NO_EVENT; // assume no errors
	MorseElementState_t NextState = CurrentState;
	
  
	switch(CurrentState) {	
		
		case InitMorseElements:
			if(ThisEvent.EventType == ES_INIT) {
				NextState = CalWaitForRise;
			}
			break;
		
			
		case CalWaitForRise:
			if(ThisEvent.EventType == RisingEdge) {
				TimeOfLastRise = ThisEvent.EventParam;
				NextState = CalWaitForFall;
			}
			
			if(ThisEvent.EventType == CalibrationCompleted) {
				printf("Length of Dot = %d\n", LengthOfDot);
        NextState = EOC_WaitRise;
			}
			break;
		
			
		case CalWaitForFall:
			if(ThisEvent.EventType == FallingEdge) {
				TimeOfLastFall = ThisEvent.EventParam;
				NextState = CalWaitForRise;
				TestCalibration();
			}
			break;
		
			
		case EOC_WaitRise:
			if(ThisEvent.EventType == RisingEdge) {
				// printf("RisingEdge\n");
				TimeOfLastRise = ThisEvent.EventParam;
				NextState = EOC_WaitFall;
				CharacterizeSpace();
			}
      
      if(ThisEvent.EventType == DBButtonDown) {
        NextState = CalWaitForRise;
        FirstDelta = 0;
        printf("Calibration Button Pressed\n");
				printf("First Delta = %d\n", FirstDelta);
      }
			break;
		
			
		case EOC_WaitFall:
			if(ThisEvent.EventType == FallingEdge) {
				// printf("FallingEdge\n");
				TimeOfLastFall = ThisEvent.EventParam;
				NextState = EOC_WaitRise;
			}
			
			if(ThisEvent.EventType == DBButtonDown) {
				NextState = CalWaitForRise;
				FirstDelta = 0;
        printf("Calibration Button Pressed\n");
				printf("First Delta = %d\n", FirstDelta);
			}
			
			if(ThisEvent.EventType == EOCDetected) {
				NextState = DecodeWaitFall;
			}
			break;
		
			
		case DecodeWaitRise:
			// printf("In DecodeWaitWaitRise state");
			if(ThisEvent.EventType == RisingEdge) {
				// printf("RisingEdge");
				TimeOfLastRise = ThisEvent.EventParam;
				NextState = DecodeWaitFall;
				CharacterizeSpace();
			}
			
			if(ThisEvent.EventType == DBButtonDown) {
				NextState = CalWaitForRise;
				FirstDelta = 0;
        printf("Calibration Button Pressed\n");
				printf("First Delta = %d\n", FirstDelta);
			}
			break;
		
			
		case DecodeWaitFall:
			// printf("In DecodeWaitWaitFall state");
			if(ThisEvent.EventType == FallingEdge) {
				TimeOfLastFall = ThisEvent.EventParam;
				NextState = DecodeWaitRise;
				CharacterizePulse();
			}
			
			if(ThisEvent.EventType == DBButtonDown) {
				NextState = CalWaitForRise;
				FirstDelta = 0;
        printf("Calibration Button Pressed\n");
				printf("First Delta = %d\n", FirstDelta);
			}
			break;
		

	}	
  CurrentState = NextState;
	return ReturnEvent;
}




/*
*/
void TestCalibration() {
	uint16_t SecondDelta;
  // printf("TestCalibration");
	// If calibration is just starting (FirstDelta is 0)
	if(FirstDelta == 0) {
		FirstDelta = TimeOfLastFall - TimeOfLastRise;
	}
	else {
		SecondDelta = TimeOfLastFall - TimeOfLastRise;
       
		if( (100.0 * FirstDelta / SecondDelta) <= 34.0) {
			LengthOfDot = FirstDelta;
			
			struct ES_Event ThisEvent;
			ThisEvent.EventType = CalibrationCompleted;
			ES_PostToService( MyPriority, ThisEvent);
		}
		else if( (100.0 * FirstDelta / SecondDelta) > 300.0) {
			LengthOfDot = SecondDelta;
			
			struct ES_Event ThisEvent;
			ThisEvent.EventType = CalibrationCompleted;
			ES_PostToService( MyPriority, ThisEvent);			
		}
		else { // Prepare for next pulse
			FirstDelta = SecondDelta;
		}
	}
	
}




/*
CharacterizeSpaceTakes no parameters, returns nothing.
Posts one of EOCDetected Event, EOWDetected Event, 
BadSpace Event as appropriateon good dot-space, does nothing
*/
void CharacterizeSpace() {

	uint16_t LastInterval;
	struct ES_Event Event2Post;
	
	LastInterval = TimeOfLastRise - TimeOfLastFall;

  // If LastInterval not OK for a Dot Space
      // If LastInterval OK for a Character Space (3 X DotLength)
          // PostEvent EOCDetected Event to Decode Morse Service & Morse Elements Service
      // Else
          // If LastInterval OK for Word Space (7 X DotLength)
              // PostEvent EOWDected Event to Decode Morse Service
          // Else 
              // PostEvent BadSpace Event to Decode Morse Service

	uint16_t LengthRatio = abs(LastInterval/ LengthOfDot);
  // printf("LastInterval = %d\n", LastInterval);
  // printf("Length of Dot = %d\n", LengthOfDot);
  // printf("LengthRatio = %d\n", LengthRatio);
  
  if( LengthRatio >= 3 ) {
    if( LengthRatio >= 3 && LengthRatio < 7 ) {
      // printf(" ");
      Event2Post.EventType = EOCDetected;
      PostMorseElementService(Event2Post);
      PostDecodeMorseService(Event2Post);
      
    }
    else {
      if( LengthRatio >= 7 ) {
        Event2Post.EventType = EOWDetected;
        PostDecodeMorseService(Event2Post);
        
      }
      else {
        Event2Post.EventType = BadSpace;
        PostDecodeMorseService(Event2Post);
        
      }
    }
    
  }
  
  
  
	return;
}



/*
CharacterizePulseTakes no parameters, returns nothing.
Posts one of DotDetectedEvent, DashDetectedEvent, BadPulseEvent,
*/
void CharacterizePulse() {
	// printf("Characterize Pulse is called\n");
	uint16_t LastPulseWidth;
	struct ES_Event Event2Post;
	
	LastPulseWidth = TimeOfLastFall - TimeOfLastRise;

  // If LastPulseWidth OK for a dot
      // PostEvent DotDetected Event to Decode Morse Service
  // Else
      // If LastPulseWidth OK for dash
          // PostEvent DashDetected Event to Decode Morse Service
      // Else
          // PostEvent BadPulse Event to Decode Morse Service

  uint16_t LengthRatio = abs(LastPulseWidth/ LengthOfDot);
  // printf("LastInterval = %d\n", LastPulseWidth);
  // printf("Length of Dot = %d\n", LengthOfDot);
  // printf("LengthRatio = %d\n", LengthRatio);
  
  if( LengthRatio < 2 ) {
    // printf(".");
    Event2Post.EventType = DotDetected;
    PostDecodeMorseService(Event2Post);
  }
  else {
    if ( LengthRatio >= 2) {
      // printf("-");
      Event2Post.EventType = DashDetected;
      PostDecodeMorseService(Event2Post);
    }
    else {
      Event2Post.EventType = BadPulse;
      PostDecodeMorseService(Event2Post);
    }
  }
	return;
}





bool PostMorseElementService( ES_Event_t ThisEvent )
{
  return ES_PostToService( MyPriority, ThisEvent);
}







#ifdef TEST


int main(void) {
  TERMIO_Init();
	printf("%d", InitializeMorseElements(8));
	
  while(!kbhit()) {
    CheckMorseEvents();
  }


}
#endif
