/****************************************************************************
 Module
   MorseElementsService.c

Pseudo-code for the Morse Elements module (a service that implements a state 
machine)Data private to the module: MyPriority, CurrentState, TimeOfLastRise, 
TimeOfLastFall, LengthOfDot, FirstDelta, LastInputState
****************************************************************************/
// #define TEST

// the common headers for C99 types 
#include <stdint.h>
#include <stdbool.h>
#include <termio.h>
#include <string.h>

// the headers to access the GPIO subsystem
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_sysctl.h"

// the headers to access the TivaWare Library
#include "driverlib/sysctl.h"
#include "driverlib/pin_map.h"
#include "driverlib/gpio.h"
#include "driverlib/timer.h"
#include "driverlib/interrupt.h"

/* include header files for the framework
*/
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "ES_DeferRecall.h"
#include "ES_ShortTimer.h"

#include "BITDEFS.H"

#include "DecodeMorse.h"
#include "LCDService.h"

// readability defines
#define ALL_BITS (0xff << 2)
#define MAX_MORSE_STRING_SIZE 30

/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this service.They should be functions
   relevant to the behavior of this service
*/
char DecodeMorseString(void);
/*
void TestCalibration(void);
void CharacterizeSpace(void);
void CharacterizePulse(void);
*/

/*---------------------------- Module Variables ---------------------------*/
// with the introduction of Gen2, we need a module level Priority variable
static char MorseString[MAX_MORSE_STRING_SIZE];
static char legalChars[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890?.,:'-/()\"= !$&+;@_";
static char morseCode[][8] ={ ".-","-...","-.-.","-..",".","..-.","--.","....","..",".---","-.-",".-..","--","-.","---",".--.","--.-",".-.","...","-","..-","...-",".--","-..-","-.--","--..",".----","..---","...--","....-",".....","-....","--...","---..","----.","-----","..--..",".-.-.-","--..--","---...",".----.","-....-","-..-.","-.--.-","-.--.-",".-..-.","-...-","-.-.--","...-..-",".-...",".-.-.","-.-.-.",".--.-.","..--.-"};
static uint8_t MyPriority; 


/*
*/  
bool InitMorseDecode(uint8_t Priority) {
  bool ReturnVal = true;
  MyPriority = Priority;
  
  strcpy(MorseString, "");
  
  return ReturnVal;
}







/*
*/
ES_Event_t RunMorseDecode( ES_Event_t ThisEvent ) {
  struct ES_Event ReturnEvent;
  ReturnEvent.EventType = ES_NO_EVENT;
  
  // printf("In RunMorseDecode\n");
  
  if(ThisEvent.EventType == DotDetected) {
    printf(".");
    // If there is room for another Morse element in the internal representation
        // Add a Dot to the internal representation
    // Else
        // Set ReturnValue to ES_ERROR with param set to indicate this location
    if(strlen(MorseString) < MAX_MORSE_STRING_SIZE) {
      strcat(MorseString, ".");
      printf("%s\n", MorseString);
    }
    else {
      ReturnEvent.EventType = ES_ERROR;
      ReturnEvent.EventParam = strlen(MorseString);
    }
  }
  
  if(ThisEvent.EventType == DashDetected) {
    printf("-");
    // If there is room for another Morse element in the internal representation
        // Add a Dash to the internal representation
    // Else
        // Set ReturnValue to ES_ERROR with param set to indicate this location
    
    if(strlen(MorseString) < MAX_MORSE_STRING_SIZE) {
      strcat(MorseString, "-");
    }
    else {
      ReturnEvent.EventType = ES_ERROR;
      ReturnEvent.EventParam = strlen(MorseString);
    }
  }
  
  if(ThisEvent.EventType == EOCDetected) {
    printf("EOCDetected");
    // call DecodeMorse to try and match current MorseString
    char ReturnChar = DecodeMorseString();
    
    // Print to LCD the decoded character
    struct ES_Event Event2LCDService;
    Event2LCDService.EventType = ES_LCD_PUTCHAR;
    Event2LCDService.EventParam = ReturnChar;
    PostLCDService(Event2LCDService);
    
    // Clear (empty) the MorseString variable
    strcpy(MorseString, "");
  }
  
  if(ThisEvent.EventType == EOWDetected) {
    printf(" ");
    // call DecodeMorse to try and match current MorseString
    char ReturnChar = DecodeMorseString();
    
    // Print to LCD the decoded character
    struct ES_Event Event2LCDService;
    Event2LCDService.EventType = ES_LCD_PUTCHAR;
    Event2LCDService.EventParam = ReturnChar;
    PostLCDService(Event2LCDService);
    
    // Print to the LCD a space
    ReturnChar = ' ';
    Event2LCDService.EventParam = ReturnChar;
    PostLCDService(Event2LCDService);
    
    // Clear (empty) the MorseString variable
    strcpy(MorseString, "");
  }
  
  if(ThisEvent.EventType == BadSpace) {
    strcpy(MorseString, "");
  }
  
  if(ThisEvent.EventType == BadPulse) {
    strcpy(MorseString, "");
  }
  
  if(ThisEvent.EventType == DBButtonDown) {
    strcpy(MorseString, "");
  }
  
  
  return ReturnEvent;
}







/*
*/
char DecodeMorseString(void) {
  
  for(int i = 0; i < strlen(legalChars); i++) {
    if( strcmp(MorseString, morseCode[i]) == 0) {
      return legalChars[i];
    }
  }
  
  return '~';
}




bool PostDecodeMorseService( ES_Event_t ThisEvent )
{
  return ES_PostToService( MyPriority, ThisEvent);
}






#ifdef TEST

int main(void) {
  TERMIO_Init();
  
  printf("In DecodeMorse.c");
  printf("\n");
  /*
  for(int i = 0; i < strlen(legalChars); i++) {
    printf("%s", morseCode[i]);
  }
  */
  
  char cha[10] = "k";
  strcat(cha, "al");
  printf("%s\n", cha);
  
  printf("%d\n", strcmp("Hel", "Helo"));



}
#endif
