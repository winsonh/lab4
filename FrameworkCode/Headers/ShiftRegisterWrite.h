#ifndef Shift_Register_H
#define Shift_Register_H

#include <stdint.h>

void SR_Init(void);
uint8_t SR_GetCurrentRegister(void);
void SR_Write(uint8_t NewValue);
void PulseSCLK(void);

#endif   /* Shift_Register_H */
