/****************************************************************************
 
  Header file for Test Harness Service0 
  based on the Gen 2 Events and Services Framework

 ****************************************************************************/

#ifndef DecodeMorse_Service_H
#define DecodeMorse_Service_H

// Event Definitions
#include "ES_Configure.h" /* gets us event definitions */
#include "ES_Types.h"     /* gets bool type for returns */
#include "ES_Events.h"    

// typedefs for the states
// State definitions for use with the query function
/*
typedef enum { InitMorseElements,
							 CalWaitForRise,
							 CalWaitForFall,
							 EOC_WaitRise, 
							 EOC_WaitFall, 
							 DecodeWaitRise, 
							 DecodeWaitFall, 
							} MorseElementState_t ;
*/

// Public Function Prototypes

bool InitMorseDecode(uint8_t Priority) ;
ES_Event_t RunMorseDecode( ES_Event_t ThisEvent) ;
bool PostDecodeMorseService( ES_Event_t ThisEvent );

#endif /* Morse_Elements_Service_H */
